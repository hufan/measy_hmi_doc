## 7. DBUS API介绍

---

&emsp;&emsp;本章节将会列出MYIR Dbus Library库中的接口和网络管理服务Connman提供的dbus接口，并对其中的DBUS接口函数以及信号进行说明。关于MYIR Dbus Library的详细内容用户可以自行参考MxDbus源码。MxDbus中所用到的dbus Method和Signal都在mxdbus目录下的mxde.xml文件中可以看到，在编译本地HMI应用的过程中会生成相应的QT信号和槽，这个过程可以参考源码。

