### 2.1 LED

This example shows how to use the LED application in the MEasy HMI to control the LED on the development board. For details, refer to the source code mxled.

Software Environment**：**

* LED Application

Hardware Environment**：**

* LED on development board

| Board | LED1 | LED2 | LED3 | LED4 |
| :--- | :--- | :--- | :--- | :--- |
| MYD-C437X | D36  core board | D34 base board | D35 base board | D36 base board |
| MYD-C437X-PRU | D36 core board | D20 base board | D19 base board | D18 base board |
| Rico Board | D23 | D24 | D25 | D26 |

UI Description**：**

![jpg](imagech/2-1-led.jpg)

Status bar: indicates the status of the current LED

Switch bar: button to control the switch of the LED

Position bar: Description of the current silk screen location on the LED redevelopment board.

Test steps:

* The corresponding LED light is controlled by operating the button corresponding to the switch bar.



