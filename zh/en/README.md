# MEasy HMI V1.0 Development Guide


##Introduction  
----------------------------------

This article mainly describes how to build and run MEasy HMI V1.0 application program on the development boards of `MYiR`. Including the establishment of the development environment, source code compilation, instance analysis of MEasy HMI applications, DBUS library API introduction.

This document is suitable for embedded linux development engineers QT, Python Web-backend and front-end development engineers with certain development experience.

**Version History:**

| Version| Description| Time |
|---------|-------------|------|
| V1.0 | Initial Version | 2018.5.1 |

**Hardware Version:**
This document applies to AM335X, AM437X and i.MX6UL series development boards of MYIR currently.


> Note: The default password for root of the embedded Linux system is not set.
   

<!-- toc -->

