### 2.3 RS485

This routine demonstrates how to use the RS485 application in MEasy HMI to configure the RS485 device and RS485 transceiver data test of the development board. For details, refer to the source code mxrs485.

Software Environment**：**

* RS485 Application

Hardware Environment**：**

* Two MYD AM437X series development board .
* Data cable connects the RS485 interface of two boards，485A&lt;-&gt;485A，485B&lt;-&gt;485B，GND&lt;-&gt;GND.  

| Board | Interface | Data Cable |
| :--- | :--- | :--- |
| MYD-C437X | J15  485\_A 485\_B GND | DuPont Line |
| MYD-C437X-PRU | J10  485A 485B GND | DuPont Line |
| Rico Board | NONE | NONE |

UI Description**：**

![](imagech/2-3-rs485.jpg)

Setting group box: Set RS485 parameters and open RS485.

Send group box: Send RS485 data.

Receive group box: Accept RS485 data.

> Note: Click on the edit box in the Send group box will pop up the soft keyboard, after you enter the data you need to click the blue Close button on the soft keyboard to close the soft keyboard, then click the send button to send the data out, after sending the edit box The data is automatically cleared.

Test steps:

* Connecting RS485 Interface of Two Development Boards with DuPont Cable

* Start the RS485 applications in the MEasy HMI in their respective development boards

* Configure the RS485 configuration group box parameters of the development board, the port may be different, to ensure that the two development board baud rate, parity, data bits and stop bits are consistent.

* After the configuration is complete, click the Open button, and then send and receive data tests on the two development boards.



