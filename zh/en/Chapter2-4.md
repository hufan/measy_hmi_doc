### 2.4 CAN BUS

This routine demonstrates how to use the CAN bus application in the MEasy HMI to configure the CAN bus device of the development board and the CAN bus to send and receive data tests. For details, refer to the source code mxcan.

Software Environment**：**

* CAN BUS Application

Hardware Environment**：**

* Two MYD AM437X series development board .
* Data cable connects the two boards' CAN interface，CANH0&lt;-&gt;CANH0，CANL0&lt;-&gt;CANL0，GND&lt;-&gt;GND

| Board | Interface | Date Cable |
| :--- | :--- | :--- |
| MYD-C437X | J15 CANH0 CANL0 CANH1 CANL1GND | DuPont Line |
| MYD-C437X-PRU | J10 CANH CANL GND | DuPont Line |
| Rico Board | NONE | NONE |

UI Description**：**

![](imagech/2-4-can.jpg)

Setting group box: Set CAN parameters and turn on CAN

Send group box: Send CAN data, including CAN ID and CAN data

Receive group box: introduce CAN data

> Note: 1. Click the edit box in the sending group box to pop up the soft keyboard. After entering the data, you need to click the blue Close button on the soft keyboard to close the soft keyboard. Then click the send button to send the data out. After editing, the edit box is sent. The data in the data is automatically cleared.
>
> 2.The sending data consists of CAN ID and CAN data: The width of CAN ID data is three digits, such as 001, 011, 111; the CAN data width is two digits, for example, 01, 11 and can data is up to 8 bytes. Each byte Spaces are separated by spaces, for example 01 02 03 04 05 06 07 08.  
> 3.loopback mode part of the development board does not support

Test steps:

* Using the DuPont cable to connect the two development board's CAN interface

* Launch CAN bus applications in MEasy HMI in their respective development boards

* Configure the parameters of the CAN setup group box of each development board. The ports may not be the same. The baud rate of the two development boards must be the same.

* After the configuration is complete, click the Open button, and then send and receive data tests on the two development boards.



