### 4.2 Serial 

&emsp;&emsp;This example shows how to use Web HMI to configure the RS232 on the development board, and then test the data send and receive. For details,refer to the source code.

** Hardware Environment：**

&emsp;&emsp; Hardware Connections Reference Chapter2.2


- Select the configured parameters first, then click the Open button
- Modify the configuration parameters will automatically turn off the device, need to open again 


** Note：**    

&emsp;&emsp;Port options in the interface can be modified or added in the board_cfg.json configuration file.  

** UI: **
{% raw %}
<div  align="center" >
<img src="/imagech/WEB-RS232.jpg",alt="cover", width=480 >
</div>
<div align="center" > Figure4-2-1 Web HMI Test RS232 </div>
<p></p>
{% endraw %}  


