### 2.8 System Info

This routine demonstrates how to use the system information application in the MEasy HMI to view the hardware and software information of the development board. For details, refer to the source code mxinfo.

Software Environment**：**

* System Info Application

Hardware Environment**：**

* One MYD AM437X series development board 

UI Description**：**

![](imagech/2-8-system.jpg)

Use steps:

* Open the System Info application in MEasy HMI.



