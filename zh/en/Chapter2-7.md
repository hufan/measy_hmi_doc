### 2.7 MYIR Support

This routine demonstrates how to use the MYIR Support application in MEasy HMI to obtain contact information with us. For details, please refer to the source code mxsupport.

Software Environment**：**

* MYIR Support Application

Hardware Environment**：**

* One MYD AM437X series development board 

UI Description**：**

![](imagech/2-7-support.jpg)

Use steps:

* Open the MYIR Support application in MEasy HMI.



