### 2.5 Ethernet

This example shows how to use the Ethernet application in the MEasy HMI to configure the development board's network port and test network port connectivity. For details, refer to the source code mxnet.

Software Environment**：**

* Ethernet Application

Hardware Environment**：**

* One router that can provide DHCP service

* One MYD AM437X series development board 

| Board | Interface |
| :--- | :--- |
| MYD-C437X | J11 EHT1 J10 ETH2 |
| MYD-C437X-PRU | J6 ETH1 J26 PRU-ETH0 J27 PRU-ETH1 |
| Rico Board | J5 ETHERNET |

UI Description**：**

![](imagech/2-5-net.jpg)

![](imagech/2-5-1-net.jpg)

Tab page: Function page corresponding to network card

IP Information Page: Contains settings group box and information group box

Ping Test: Test Network Connectivity Pages

> Note: 1. The tab page is dynamically created. If you do not see any interface without plugging in the network cable, insert several network cables into the network port. Several tab pages will be created. Similarly, removing the cable will delete the corresponding tab. Bookmark page.
>
> 2.When the IP acquisition mode is switched to Manual mode, the IP address, subnet mask, and gateway input boxes will pop up, which can be used to configure IP manually.
>
> 3.In the manual mode of IP acquisition, click the IP address, subnet mask, and the input box edit box of the gateway will pop up the soft keyboard. After entering the data, you need to click the blue Close button on the soft keyboard to close the soft keyboard. Click the OK button to configure the IP address, subnet mask, and gateway information. After the configuration is complete, the data in the edit box is automatically cleared.

Test steps:

* Insert the network cable into the network port of the development board.

* Open the Ethernet test application in the MEasy HMI and check whether the information group box has successfully obtained the IP information.

* Switch to Ping test page to test network connectivity.



