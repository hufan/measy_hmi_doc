## 2. HMI Introduction

---

This section mainly introduces the details of the use and use of each APP in the MEasy HMI.

Software Environment**：**

* u-boot-2016.05
* linux-4.1.18
* File system with QT5 operating environment
* MEasy HMI V1.0 application

The above software has been programmed into the corresponding development board.

Hardware Environment**：**

* MY-TFT070RV2 resistive screen/MY-TFT070CV2 capacitive screen/HDMI Displays
* MYD-C437X Development Board/MYD-C437X-PRU Development Board/Rico Board Development Board

Hardware connection method:

| Board | LCD Interface |
| :--- | :--- |
| MYD-C437X | J8  LCD\_16bit |
| MYD-C437X-PRU | J20 LCD\_16B |
| Rico Board | J9  LCD |



