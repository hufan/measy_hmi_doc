### 4.1 LED 

&emsp;&emsp;Web HMI will automatically read the number and status of the LED on the development board and display it. Click the LED_ON or LED_OFF button to control the light on and off.

** Hardware environment：**

&emsp;&emsp; Hardware Connections Reference Chapter2.1


** UI: **   
{% raw %}
<div  align="center" >
<img src="/imagech/WEB-LED.jpg",alt="cover", width=480 >
</div>
<div align="center" >Figure4-1-1 Web HMI Test LED </div>
<p></p>
{% endraw %}  

