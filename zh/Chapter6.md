## 6 MEasy HMI应用集成  

---

&emsp;&emsp;在前面的章节中我们介绍了MEasy HMI在目标板上的目录结构，以及本地HMI和Web HMI的运行时环境，开发过程。本章将重点介绍如何将MEasy HMI应用集成到目标板系统当中，使之开机就能启动。


### 6.1 i.MX6UL系列开发板上集成MEasy HMI应用   
 
&emsp;&emsp;Yocto中一个软件包是放在bb文件里的，然后非常多的bb文件集成一个recipe，然后很多的recipe又组成一个meta layer。因此，要加入一个软件包可以在recipe以下加入一个bb（bitbake配置文件）。下面介绍在系统中加入web-demo,
在目录fsl-release-yocto/sources/meta-myir-imx6ulx/recipes-myir下建立如下目录结构：

```
web-demo/
├── web-demo
│   ├── board_cfg.json
│   ├── cJSON.tar.bz2
│   ├── mxde.xml
│   ├── myir
│   └── settings.ini
└── web-demo.bb
```

&emsp;&emsp;web-demo.bb是对应执行的任务，主要工作是编译代码然后整合软件到rootfs，以shell为开发语言。  
mxde.xml、board_cfg.json、settings.ini是配置文件，MEasy HMI程序运行依赖的配置，需要一起打包到文件系统。myir目录下为web-demo的python代码。cJSON.tar.bz2是cJSON的库，此处会编译出cJSON的动态库并打包到文件系统。


&emsp;&emsp;web-demo.bb的内容如下：
```
DESCRIPTION = "web demo"
DEPENDS = "zlib glibc ncurses "
SECTION = "libs"
LICENSE = "MIT"
PV = "3"
PR = "r0"

PACKAGES = "${PN}-dbg ${PN} ${PN}-doc ${PN}-dev ${PN}-staticdev ${PN}-locale"
PACKAGES_DYNAMIC = "${PN}-locale-*"

SRC_URI = "file://cJSON.tar.bz2;md5sum=580394db958e0edfcd5ca4b25fbedf96 \
           file://myir \
           file://board_cfg.json \
           file://mxde.xml \
           file://settings.ini \
           "

LIC_FILES_CHKSUM = "file://settings.ini;md5=b2e121f7083d25452d0f7168eb7196f9"
S = "${WORKDIR}"

do_compile () {
    make
}

do_install () {
      install -d ${D}/usr/share/myir/
      install -d ${D}/lib/
      cp -S ${S}/*.so*  ${D}/lib/
      cp -r  ${S}/myir/www/  ${D}/usr/share/myir/
      install -m 0755 ${S}/board_cfg.json ${D}/usr/share/myir/
      install -m 0755 ${S}/mxde.xml ${D}/usr/share/myir/
      install -m 0755 ${S}/settings.ini ${D}/usr/share/myir/
}

FILES_${PN} = "/home/myir/ \
               /usr/share/myir/ \
               /usr/share/myir/*/* \
               /lib/ \
              "
```
&emsp;&emsp;qt-demo.bb的内容如下： 
```
SRC_URI = " file://qt-app \ 
	file://so \ 
	git://github.com/hufan/web-demo-bb;protocol=https;branch=qt-app \
    "

S = "${WORKDIR}/"
S_G = "${WORKDIR}/git"

do_package_qa () {
  echo "----" 
}

do_install () {
      install -d ${D}/usr/share/myir/
      install -d ${D}/usr/share/applications/
      install -d ${D}/usr/share/pixmaps/
      install -d ${D}/usr/lib/fonts/
      install -d ${D}/lib/
      install -d ${D}/home/myir/

      cp -r ${S_G}/applications/* ${D}/usr/share/applications/
      cp -r ${S_G}/pixmaps/* ${D}/usr/share/pixmaps/
      cp -r ${S_G}/msyh.ttc ${D}/usr/lib/fonts/
      cp  -rfav ${S}/so/*.so*  ${D}/lib/
      cp   ${S}/qt-app/*  ${D}/home/myir/

}

FILES_${PN} = "  /home/myir/ \
				 /usr/share/myir/ \
				 /usr/lib/fonts/ \
				 /lib/ \
				 /usr/share/applications/ \
				 /usr/share/pixmaps/ \
             "
#For dev packages only
INSANE_SKIP_${PN}-dev = "ldflags"
INSANE_SKIP_${PN} = "${ERROR_QA} ${WARN_QA}"

```


- SRC_URI : 指定源文件
- LIC_FILES_CHKSUM : 文件和对应的md5值
- do_compile、do_install : 执行bitbake的方法，编译源码和安装程序到文件系统
- FILES_${PN} : 添加支持的目录


&emsp;&emsp;然后还需在构建文件系统前加入web-demo.bb任务，参考 表5-1-2 修改对应文件系统的bbappend文件，添加如下内容：  

```
	...
	web-demo \
	qt-demo \
	...
```

&emsp;&emsp;最后开始构建系统，如构建带qt的文件系统，则执行命令: `bitbake fsl-image-qt5`。

&emsp;&emsp;关于文件系统的构建，可以参考MYD-Y6ULX发布的文档MYD-Y6ULX-LinuxDevelopmentGuide_zh.pdf的3.3章节。



### 6.2 AM437X,AM335X系列开发板上集成MEasy HMI应用 

&emsp;&emsp;MEasy HMI包含本地QT应用和Web端应用，开发编译完成之后，最终都是在buildroot中打包生成的文件系统。各个平台通用的部分,包括本地HMI应用和Web HMI应用都于位于`myir-buildroot/board/myir/common/HMI`目录下面。板级配置相关的文件分别位于各自的`myir-buildroot/board/myir/myd_xxx/rootfs-overlay/usr/share/myir/board_cfg.json`文件中。
```
#!/bin/sh -e
# File: board/myir/myd_xxx/post-build.sh

	cp -a board/myir/common/HMI/*  output/target
	
	...

```
&emsp;&emsp;在buildroot执行make的时候上述目录的文件都会通过执行上述`myir-buildroot/board/myir/myd_xxx/post-build.sh`脚本拷贝至文件系统对应的目录，例如`myir-buildroot/board/myir/common/HMI/home/myir`目录下的文件会拷贝至文件系统`/home/myir`目录下面。最终生成的目标文件系统位于`myir-buildroot/output/target`目录，在buildroot编译的最后阶段会根据这个目标文件系统生成各种格式的文件系统。  
