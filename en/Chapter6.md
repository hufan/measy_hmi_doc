## 6 MEasy HMI应用集成  

---

&emsp;&emsp;In the previous chapter we introduced the directory structure of the MEasy HMI on the target board, as well as the runtime environment and development process of the local HMI and Web HMI. This chapter will focus on how to integrate the MEasy HMI application into the target board system. Then the MEasy HMI starts up on the development board.


### 6.1 Integrate MEasy HMI Application on i.MX6UL Series Development Boards 
 
&emsp;&emsp;A package in Yocto is placed in the bb file, then a very large number of bb files integrate a recipe, and then many recipes form a meta layer. So, to join a soft Package can add a bb (bitbake configuration file) below recipe.
Here's how to add web-demo to your system,Create the following directory structure in the directory fsl-release-yocto/sources/meta-myir-imx6ulx/recipes-myir:
```
web-demo/
├── web-demo
│   ├── board_cfg.json
│   ├── cJSON.tar.bz2
│   ├── mxde.xml
│   ├── myir
│   └── settings.ini
└── web-demo.bb
```

&emsp;&emsp;Web-demo.bb is the corresponding task to perform. The main task is to compile the code and then integrate the software library into rootfs, using the shell as the development language. Mxde.xml, board_cfg.json, and settings.ini are the configuration files, the configuration files that the MEasy HMI runs depend on and need to be packaged together to the file system. Myir directory for the web-demo python code. cJSON.tar.bz2 is the cJSON library, the cJSON dynamic library will be compiled and packaged into the file system.

&emsp;&emsp;The web-demo.bb code is as follows：
```
DESCRIPTION = "web demo"
DEPENDS = "zlib glibc ncurses "
SECTION = "libs"
LICENSE = "MIT"
PV = "3"
PR = "r0"

PACKAGES = "${PN}-dbg ${PN} ${PN}-doc ${PN}-dev ${PN}-staticdev ${PN}-locale"
PACKAGES_DYNAMIC = "${PN}-locale-*"

SRC_URI = "file://cJSON.tar.bz2;md5sum=580394db958e0edfcd5ca4b25fbedf96 \
           file://myir \
           file://board_cfg.json \
           file://mxde.xml \
           file://settings.ini \
           "

LIC_FILES_CHKSUM = "file://settings.ini;md5=b2e121f7083d25452d0f7168eb7196f9"
S = "${WORKDIR}"

do_compile () {
    make
}

do_install () {
      install -d ${D}/usr/share/myir/
      install -d ${D}/lib/
      cp -S ${S}/*.so*  ${D}/lib/
      cp -r  ${S}/myir/www/  ${D}/usr/share/myir/
      install -m 0755 ${S}/board_cfg.json ${D}/usr/share/myir/
      install -m 0755 ${S}/mxde.xml ${D}/usr/share/myir/
      install -m 0755 ${S}/settings.ini ${D}/usr/share/myir/
}

FILES_${PN} = "/home/myir/ \
               /usr/share/myir/ \
               /usr/share/myir/*/* \
               /lib/ \
              "
```
&emsp;&emsp;The qt-demo.bb code is as follows： 
```
SRC_URI = " file://qt-app \ 
	file://so \ 
	git://github.com/hufan/web-demo-bb;protocol=https;branch=qt-app \
    "

S = "${WORKDIR}/"
S_G = "${WORKDIR}/git"

do_package_qa () {
  echo "----" 
}

do_install () {
      install -d ${D}/usr/share/myir/
      install -d ${D}/usr/share/applications/
      install -d ${D}/usr/share/pixmaps/
      install -d ${D}/usr/lib/fonts/
      install -d ${D}/lib/
      install -d ${D}/home/myir/

      cp -r ${S_G}/applications/* ${D}/usr/share/applications/
      cp -r ${S_G}/pixmaps/* ${D}/usr/share/pixmaps/
      cp -r ${S_G}/msyh.ttc ${D}/usr/lib/fonts/
      cp  -rfav ${S}/so/*.so*  ${D}/lib/
      cp   ${S}/qt-app/*  ${D}/home/myir/

}

FILES_${PN} = "  /home/myir/ \
				 /usr/share/myir/ \
				 /usr/lib/fonts/ \
				 /lib/ \
				 /usr/share/applications/ \
				 /usr/share/pixmaps/ \
             "
#For dev packages only
INSANE_SKIP_${PN}-dev = "ldflags"
INSANE_SKIP_${PN} = "${ERROR_QA} ${WARN_QA}"

```


- SRC_URI : Specify the source file 
- LIC_FILES_CHKSUM : File and corresponding md5 values 
- do_compile、do_install : Perform bitbake method, compile source code and install program to file system 
- FILES_${PN} : Add a supported directory


&emsp;&emsp;Then you need to add the web-demo.bb task before building the file system. Refer to Table 5-1-2 to modify the bbappend file of the corresponding file system and add the following content:
```
	...
	web-demo \
	qt-demo \
	...
```

&emsp;&emsp;Finally start building the system, such as building a file system with qt, then execute the command: `bitbake fsl-image-qt5`

&emsp;&emsp;For the construction of the file system, refer to the Chapter3.3  of the document MYD-Y6ULX-LinuxDevelopmentGuide_en.pdf published by MYD-Y6ULX.



### 6.2 AM437X,AM335X系列开发板上集成MEasy HMI应用 

&emsp;&emsp;MEasy HMI包含本地QT应用和Web端应用，开发编译完成之后，最终都是在buildroot中打包生成的文件系统。各个平台通用的部分,包括本地HMI应用和Web HMI应用都于位于`myir-buildroot/board/myir/common/HMI`目录下面。板级配置相关的文件分别位于各自的`myir-buildroot/board/myir/myd_xxx/rootfs-overlay/usr/share/myir/board_cfg.json`文件中。
```
#!/bin/sh -e
# File: board/myir/myd_xxx/post-build.sh

	cp -a board/myir/common/HMI/*  output/target
	
	...

```
&emsp;&emsp;在buildroot执行make的时候上述目录的文件都会通过执行上述`myir-buildroot/board/myir/myd_xxx/post-build.sh`脚本拷贝至文件系统对应的目录，例如`myir-buildroot/board/myir/common/HMI/home/myir`目录下的文件会拷贝至文件系统`/home/myir`目录下面。最终生成的目标文件系统位于`myir-buildroot/output/target`目录，在buildroot编译的最后阶段会根据这个目标文件系统生成各种格式的文件系统。  
