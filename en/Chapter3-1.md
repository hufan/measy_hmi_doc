### 3.1 Setting up the environment

The MEasy HMI environment includes the development environment on the development board and the qmake development environment on ubuntu.

The running environment on the development board and the qmake development environment on ubuntu are compiled by buildroot. For details, please refer to the following table sections:

| Board | Document Section |
| :--- | :--- |
| MYD-C437X | MYD-AM437X Series Linux 4.1.18 Development Guide.pdf  3.4 Build QT |
| MYD-C437X-PRU | MYD-AM437X Series Linux 4.1.18 Development Guide.pdf 3.4 Build QT |
| Rico Board | Rico Board Linux 4.1.18 Development Guide.pdf  3.4 Build QT |

The QT5 development tool QT Creator installation process directly refers to the chapters of its documentation as follows:

| Board | Document Section |
| :--- | :--- |
| MYD-C437X | MYD-AM437X Series Linux 4.1.18 Development Guide.pdf  5.1 Install QT Creator |
| MYD-C437X-PRU | MYD-AM437X Series Linux 4.1.18 Development Guide.pdf  5.1 Install QT Creator |
| Rico Board | Rico Board Linux 4.1.18 Development Guide.pdf  5.1 Install QT Creator |

QT5 development tool QT Creator configuration process directly refers to the chapter of its document as follows:

| Board | Document Section |
| :--- | :--- |
| MYD-C437X | MYD-AM437X Series Linux 4.1.18 Development Guide.pdf  5.2  Config QT Creator |
| MYD-C437X-PRU | MYD-AM437X Series Linux 4.1.18 Development Guide.pdf 5.2 Config QT Creator |
| Rico Board | Rico Board Linux 4.1.18 Development Guide.pdf  5.2 Config QT Creator |



