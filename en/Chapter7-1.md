### 7.1 LED

```
                <method name="getLedList">
                    <arg name="leds" type="s" direction="out"/>
                </method>
                <method name="setLedBrightress">
                    <arg name="led" type="s" direction="in"/>
                    <arg name="brightness" type="i" direction="in"/>
                    <arg name="result" type="i" direction="out"/>
                </method>
                <signal name="sigLedBrightnessChanged">
                    <arg name="message" type="s" direction="out"/>
                </signal>
```

Method：

getLedList   Method of get the name and status of all lights on the board

Return：

| Name | Type | Explain | Example |
| :--- | :--- | :--- | :--- |
| leds | QString | Returns the name and status of all lights | "led1 0 \n led2  0 \n" |

Method：

setLedBrightress  Method of  set the state of the LED

Input：

| Name | Type | Explain | Example |
| :--- | :--- | :--- | :--- |
| led | Qstring | led name | "led1" |
| brightness | int | led status 0 is off 1 is on | 1 |

Return：

| Name | Type | Explain | Example |
| :--- | :--- | :--- | :--- |
| result | int | Successful execution returns 0 | 0 |

Signal：

sigLedBrightnessChanged  Signal of led status has changed

Return：

| Name | Type | Explain | Example |
| :--- | :--- | :--- | :--- |
| message | Qstring | The status and name of the light. | "led1 1" |



