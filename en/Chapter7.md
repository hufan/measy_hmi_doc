## 7. DBUS API Introduction

---

This chapter focuses on the interface in the MYIR Dbus Library and the dbus interface provided by Connman, a network management service.

The interface of MYIR Dbus Library is also created based on dbus. Here we directly introduce the interface of dbus. Users of library interface can refer to the source code _mxdbus_. The dbus Method and Signal used in the library can be seen in the _mxde.xml_ file in the _mxdbus _directory. During the compilation process, the corresponding QT signals and slots are generated. This process can refer to the source code.

