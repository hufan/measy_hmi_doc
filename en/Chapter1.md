## 1. HMI Framework Introduction

---

MEasy HMI is a set of human-machine interfaces which contains a local HMI based on QT5 and a Web HMI based on Python2 back-end and HTML5. The hardware part includes: processor, display unit, input unit, communication interface, data storage unit, etc. The software part includes: dbus, connman and QT5 applications, python, tornado and other components.

MEasy HMI block diagram is shown as below：


{% raw %}
<div  align="center" >
<img src="/imagech/hmi_framework.jpg",alt="cover", width=720 >
</div>

{% endraw%}


{% raw %}
<div align="center" > Figure 1-1 MEasy HMI Framework </div>
<p></p>
{% endraw %}  


D-Bus is an advanced inter-process communication mechanism that is provided by the freedesktop.org project and is distributed under the GPL license. The main purpose of D-Bus is to provide communication for processes in the Linux desktop environment, and to pass Linux desktop environment and Linux kernel events as messages to the process.

More details about dbus can be found here [https://www.freedesktop.org/wiki/Software/dbus/](https://www.freedesktop.org/wiki/Software/dbus/)

The MEasy HMI uses D-Bus as the access interface for the QT application and the underlying hardware. The MYIR provides a complete set of control and communication interfaces for RS232, RS485, CAN, and LED hardware and encapsulates the interface into a library for external use based on D-BUS Method and Signal \(Chapter 7 describes these methods and Signal\).

Connman is software for managing network devices running embedded linux devices. Connman is a fully modular system that can be expanded by plug-in to support the management of EtherNet, WIFI, 3G/4G, Bluetooth and other network devices. .

For more details on Connman, please refer to [https://01.org/en/node/2207](https://01.org/en/node/2207)

The MEasy HMI uses Connman as the EtherNet access interface to manage EtherNet by calling the D-bus based Method and Signal provided by the connman service \(Chapter 7 introduces these methods and signals\).

