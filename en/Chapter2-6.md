### 2.6 Task Manager

This routine demonstrates how to use the task manager application in the MEasy HMI to view system resource status and process information. For details, refer to the source mxtaskmanager.

Software Environment**：**

* Task Manager Application

Hardware Environment**：**

* One MYD AM437X series development board 

UI Description**：**

![](imagech/2-6-task.jpg)

![](imagech/2-6-1-task.jpg)

Performance Information Page: Contains current processor usage, current memory usage, and current storage space usage.

Process information page: Displays all processes and process status information running on the current development board.

> Note: The storage space only shows the size of the root partition and does not represent the space of the entire storage device.

Test steps:

* Open the Task Manager application in the MEasy HMI to view related performance information and process information.



