### 4.5 EtherNet 

&emsp;&emsp;This example shows how to use Web HMI to manage the network on the development board. For details,refer to the source code,using the pyconnman component.

** Hardware Environment：**

&emsp;&emsp; Hardware Connections Reference Chapter2.5 


- The web page can display the network status in real time. You can also set the network information.
- When you modify the IP, you need to pay attention to it. If you modify the network card used by the web server, you will be prompted to modify the development board IP after clicking Confirm. At the same time, the web service is disconnected and the development board restarts.

** Note：**    

&emsp;&emsp;The NIC tab page in the interface is displayed only when the network cable is connected,UI：   
{% raw %}
<div  align="center" >
<img src="/imagech/WEB-NET.jpg",alt="cover", width=480 >
</div>
<div align="center" > Figure4-5-1 Web HMI EtherNet  </div>
<p></p>
{% endraw %}  
