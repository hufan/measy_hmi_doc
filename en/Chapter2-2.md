### 2.2 Serial

This example shows how to use the serial port application in MEasy HMI to configure the serial port device of the development board and serial port to send and receive data test. For details, please refer to the source code mxserial.

Software Environment**：**

* Serial Application

Hardware Environment**：**

* One MYIR AM437X series development board
* PC with serial assistant software

| Board | Interface | Data Cable |
| :--- | :--- | :--- |
| MYD-C437X | J17 UART3 | DB9 To USB |
| MYD-C437X-PRU | J12 RXD TXD GND | TTL To USB |
| Rico Board | NONE | NONE |

UI Description**：**

![](imagech/2-2-rs232.jpg)

Setting group box: set the serial port parameters and open the serial port.

Send group box: Send serial data.

Receive group box: Accepts serial data.

> Note: Click on the edit box in the Send group box will pop up the soft keyboard, after you enter the data you need to click the blue Close button on the soft keyboard to close the soft keyboard, then click the send button to send the data out, after sending the edit box The data is automatically cleared.

Test steps:

* Use a connection cable to connect the USB port on the PC and the serial port on the development board.

* Open the PC serial port assistant software, set the serial port parameters and open the serial port.

* Open the serial port application in MEasy HMI, set the same serial port parameters as the PC and open the serial port.

* Send data on the PC side and the development board side respectively, and then see if the data can be received on both sides.



