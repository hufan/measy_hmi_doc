## 4 Web HMI Introduce

---

&emsp;&emsp;This chapter mainly introduces the use of Web HMI to control the development board peripherals and precautions during use.

** Software Environment：**

* u-boot
* linux-4.1.x
* File system with Python2, tornado, python-dbus and other operating environments 
* MEasy Web HMI V1.0 Application

&emsp;&emsp;

**Hardwate Environment：**

* Prepare one of the AM437X, AM335X, and i.MX6UL series development boards 

** Note：**  

* ready: 

&emsp;&emsp;Before starting the system, set up a network environment. Any Ethernet interface on the development board is located on the same subnet as the remote host. If the development board is not connected to the network, the debug serial port will print the log waiting for the network every 1 second after the development board is started.

* Web login 

&emsp;&emsp;After the development board is powered on, after the network connection is successful, the serial port will print the IP address and port number bound to the Web HMI backend service. The log is as follows:  
```
Development server is running at http://192.168.1.100:8090/login
```
&emsp;&emsp;Enter in browser: ``` http://192.168.1.100:8090/login ``` (The IP that is filled in here is based on the actual IP address of the development board), you will enter the login screen.  Login user name and password default to admin.
{% raw %}
<div  align="center" >
<img src="/imagech/WEB-LOGIN.png",alt="cover", width=480 >
</div>
<div align="center" > Figure4-2-1 Web HMI login </div>
<p></p>
{% endraw %}

* Web language version 

&emsp;&emsp; Web HMI provides two languages, Chinese and English versions, the default is English version, in the upper right corner of the interface there is a button to switch languages (switching the language will automatically close the previously open module device).

* Synchronization  

&emsp;&emsp;The Web HMI and local HMI do data synchronization. Local HMI and Web HMI can open the same device at the same time, but they operate on the same device handle, The parameters of the device are the same. If the local HMI first opens the RS232 device, when the Web HMI is turned on again, it will read the parameters set by the local HMI and vice versa. If the development boards RS232, RS485, CAN receive data from other devices, the data can be accepted and displayed on the local HMI and Web HMI.

* Data displayed on the Web HMI RS485, RS232, and CAN interfaces is a string format

